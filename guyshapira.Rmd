---
title: Guy Shapira Curriculum Vitae - March 2022
name: Guy
surname: Shapira
position: "\nBioinformatics researcher, PhD student, Data scientist and AI spcialists"
address: "Department of Cell and Developmental Biology, Faculty of Medicine, Tel Aviv University"
www: shepp.co
email: "guyshapira@mail.tau.ac.il"
linkedin: guyshapira
date: "`r format(Sys.time(), '%B %Y')`"
aboutme: ""
output: 
  vitae::markdowncv:
    theme: kjhealy
---

```{r setup, include=FALSE}
#phone: +1 22 3333 4444
#github: mariecurie
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
library(vitae)
```

## Employment

```{r}
library(tibble)
tribble(
  ~ Degree, ~ Year, ~ Institution, ~ Where,
  "", "2016-Present", "Faculty of Medicine, Tel Aviv University", "Research Bioinformatics scientist and PhD student",
  "", "2018-2020", "Tel Aviv University", "Graduate teaching assistant",
  "", "2018-2019", "Gotsho Ltd.", "Full-stack developer",
  "Family-owned", "2014-2016", "Human Impact", "IT Developer"
) %>% 
  detailed_entries(Degree, Year, Institution, Where)
```

## Education

```{r}
library(tibble)
tribble(
  ~ Degree, ~ Year, ~ Institution, ~ Where,
  "Direct PhD program", "2021-Present", "Tel Aviv University", "PhD, Biomedical sciences",
  "", "2018-2020", "Tel Aviv University", "MSc., Biomedical sciences",
  "Transition to Bioinformatics", "2017-2018", "Tel Aviv University", "Computer Science core program",
  "", "2014-2017", "Tel Aviv University", "BSc., Biology"
) %>% 
  detailed_entries(Degree, Year, Institution, Where)
```

## Grants and awards

```{r}
tribble(
  ~Year, ~Type, ~Desc,
  "2021-2022", "Student fellow and scholarship recipient", "The Edmond J. Safra Center for Bioinformatics",
  "2021-2022", "Scholarship awarded for research of Endocannabinoid target for treatment of Parkinson's Disease", "The Aufzien Family Center for the Prevention and Treatment of Parkinson’s Disease",
  "2020", "Prize awarded for COVID-19 research", "The Ziva and Chanoch Finklestein research fund",
  "2020", "Travel grant awarded for research of Endocannabinoid target for treatment of neurodegenerative disorders", "Adams Super Center for Brain Studies",
  "2019", "Travel grant awarded for research of Endocannabinoid target for treatment of Parkinson's Disease", "The Aufzien Family Center for the Prevention and Treatment of Parkinson’s Disease"
) %>% 
  brief_entries(
    glue::glue("{Type}"),
    Year, 
    Desc
  )
```

## Published work

```{r}
library(dplyr)
#knitr::write_bib(c("vitae", "tibble"), "packages.bib")

bibliography_entries("packages.bib") %>%
  arrange(issued)
```

